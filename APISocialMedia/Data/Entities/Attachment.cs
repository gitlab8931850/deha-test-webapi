﻿namespace APISocialMedia.Data.Entities
{
    public class Attachment
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? FileUrl { get; set; }
        public long? FileSize { get; set; }
        public int PostId { get; set; }
        public Post? Post { get; set; }
    }
}
