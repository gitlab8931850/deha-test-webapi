﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace APISocialMedia.Data.Entities
{
    public class AppUser : IdentityUser
    {
        public string? FullName { get; set; }
        public DateTime? Dob {  get; set; }
        public DateTime? DateCreated { get; set; }
        public bool IsDeleted { get; set; } = false;
        public List<Post>? Posts { get; set; }
        public List<Comment>? Comments { get; set; }
        public List<VotesInPost>? VotesInPosts { get; set; }
    }
    public class UserConfiguration : IEntityTypeConfiguration<AppUser>
    {
        public void Configure(EntityTypeBuilder<AppUser> builder)
        {
            builder.HasMany(u => u.Posts)
                .WithOne(p => p.User)
                .HasForeignKey(p => p.UserId)
                .HasPrincipalKey(u => u.Id);
            builder.HasMany(u => u.Comments)
                .WithOne(c => c.User)
                .HasForeignKey(c => c.UserId)
                .HasPrincipalKey(u => u.Id);
        }
    }

}
