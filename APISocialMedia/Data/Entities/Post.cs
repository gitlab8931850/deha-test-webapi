﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace APISocialMedia.Data.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Content { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.UtcNow.AddHours(7);
        public int ViewCount { get; set; } = 0;
        public int Votes { get; set; } = 0;
        public string? UserId { get; set; }
        public AppUser? User { get; set; }
        public List<VotesInPost>? VotesInPosts { get; set; }
        public List<Comment>? Comments { get; set; }
        public List<Attachment>? Attachments { get; set; }
    }
    public class PostConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasMany(p => p.Comments)
                .WithOne(c => c.Post)
                .HasForeignKey(c => c.PostId)
                .HasPrincipalKey(p => p.Id);
            builder.HasMany(p => p.Attachments)
                .WithOne(a => a.Post)
                .HasForeignKey(a => a.PostId)
                .HasPrincipalKey(p => p.Id);
        }
    }
}
