﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace APISocialMedia.Data.Entities
{
    public class VotesInPost
    {
        public int PostId { get; set; }
        public Post? Post { get; set; }
        public string? UserId { get; set; }
        public AppUser? User { get; set; }
    }
    public class VotesInPostConfiguration : IEntityTypeConfiguration<VotesInPost>
    {
        public void Configure(EntityTypeBuilder<VotesInPost> builder)
        {
            builder.HasKey(x => new { x.PostId, x.UserId });
            builder.HasOne(x => x.Post).WithMany(p => p.VotesInPosts).HasForeignKey(x => x.PostId);
            builder.HasOne(x => x.User).WithMany(u => u.VotesInPosts).HasForeignKey(x => x.UserId);
        }
    }
}
