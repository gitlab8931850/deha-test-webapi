﻿using APISocialMedia.Data.Entities;
using Microsoft.AspNetCore.Identity;

namespace APISocialMedia.Data
{
    public class DbInitializer
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public DbInitializer(
            UserManager<AppUser> userManager,
          RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }
        public async Task Seed(IServiceProvider serviceProvider)
        {
            // Seeding role
            if (!_roleManager.Roles.Any())
            {
                await _roleManager.CreateAsync(new IdentityRole
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = AppRole.Admin,
                    NormalizedName = AppRole.Admin.ToUpper(),
                });
                await _roleManager.CreateAsync(new IdentityRole
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = AppRole.Member,
                    NormalizedName = AppRole.Member.ToUpper(),
                });
                await _roleManager.CreateAsync(new IdentityRole
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = AppRole.Manager,
                    NormalizedName = AppRole.Manager.ToUpper(),
                });
            }

            // Seeding user
            if (!_userManager.Users.Any())
            {
                var result = await _userManager.CreateAsync(new AppUser
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = "admin@gmail.com",
                    FullName = "Administator",
                    DateCreated = DateTime.UtcNow,
                    Email = "admin@gmail.com",
                    LockoutEnabled = false,
                    PhoneNumber = "0987654321",
                }, "Admin@123");
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync("admin@gmail.com");
                    if (user != null)
                        await _userManager.AddToRoleAsync(user, AppRole.Admin);
                }
            }
        }
    }
}
