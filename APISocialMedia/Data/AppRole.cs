﻿namespace APISocialMedia.Data
{
    public static class AppRole
    {
        public const string Admin = "Aministrator";
        public const string Member = "Member";
        public const string Manager = "Manager";
    }
}
