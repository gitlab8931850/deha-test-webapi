﻿using APISocialMedia.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Emit;

namespace APISocialMedia.Data
{
    public class SocialMediaDbContext : IdentityDbContext<AppUser>
    {
        public SocialMediaDbContext(DbContextOptions<SocialMediaDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                var tableName = entityType.GetTableName();
                if (tableName!.StartsWith("AspNet"))
                    entityType.SetTableName(tableName.Substring(6));
            }

            builder.ApplyConfiguration(new PostConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());
            builder.ApplyConfiguration(new VotesInPostConfiguration());
        }
        #region Db Set
        public DbSet<Post> Posts { get; set; } = null!;
        public DbSet<Attachment> Attachments { get; set; } = null!;
        public DbSet<Comment> Comments { get; set; } = null!;
        public DbSet<VotesInPost> VotesInPosts { get; set; } = null!;
        #endregion
    }
}
