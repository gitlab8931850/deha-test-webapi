﻿using APISocialMedia.Data.Entities;
using APISocialMedia.Helps;
using APISocialMedia.Services;
using APISocialMedia.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System.Drawing.Printing;
using System.Security.Claims;

namespace APISocialMedia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPostService _postService;
        private readonly UserManager<AppUser> _userManager;

        public PostsController(
            IMapper mapper, 
            IPostService postService, 
            UserManager<AppUser> userManager)
        {
            _mapper = mapper;
            _postService = postService;
            _userManager = userManager;
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAll()
        {
            var posts = await _postService.GetAsync();
            if (posts == null)
                return Ok(new ResponseObject
                {
                    Success = false,
                    Message = "Không có bài viết nào"
                });
            var postResponse = _mapper.Map<List<PostResponse>>(posts);
            return Ok(new ResponseObject
            {
                Success = true,
                Message = "Danh sach bai viet",
                Data = postResponse
            }) ;
        }
        [HttpGet("filter")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllFilter(
            string? sortOrder, 
            string? currentFilter, 
            string? searchString,
            string? userId,
            int? pageNumber, 
            int pageSize = 10
            )
        {

            var result = await _postService.GetAllFilter(sortOrder!, currentFilter!, searchString!, userId, pageNumber, pageSize);
            return Ok(result);
        }
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetById(int id, int? pageNumber, int pageSize = 5)
        {
            if(User.Identity!.IsAuthenticated == true)
                {
                var result = await _postService.UpView(id, User);
                if (result == -1) return NotFound();
            }
            var post = await _postService.GetByIdAsync(id, pageNumber, pageSize);
            if(post == null)
                return NotFound();
            return Ok(post);
        }
        [HttpPost]
        public async Task<IActionResult> AddPost(PostRequest postRequest)
        {
            if(ModelState.IsValid)
            {
                // Get user by email -> get userId
                var email = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
                var user = await _userManager.FindByEmailAsync(email!);
                var userId = user!.Id;
                // Add post
                var result = await _postService.AddPost(postRequest, userId);
                if (result == 0)
                    return StatusCode(500);
                return Ok(postRequest);
            }
            return BadRequest();
        }
        [HttpPut]
        public async Task<IActionResult> Update(PostViewModel postViewModel)
        {
            var result = await _postService.UpdatePost(postViewModel, User);
            if(result == 0)
                return StatusCode(500);
            if(result == -1)
                return StatusCode(403);
            return Ok(postViewModel);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            var result = await _postService.DeletePost(id, User);
            if (result == -1)
                return StatusCode(403);
            if (result == 0)
                return StatusCode(404);
            return Ok("Success");
        }
        [HttpPatch("Vote")]
        public async Task<IActionResult> UpVotes(int postId)
        {
            var user = await _userManager.FindByEmailAsync(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value!);
            var result = await _postService.UpVote(postId, user!.Id);
            if (result == 0)
                return NotFound();
            return Ok("Success");
        }

        //[HttpPost("Upfile")]
        //public IActionResult Upload(IList<IFormFile> file)
        //{
        //    if (file != null)
        //    {
        //        var data = new List<object>();
        //        foreach(var item in file)
        //        {
        //            var fileName = item.FileName;
        //            var fileSize = item.Length;
        //            data.Add(new { fileName, fileSize });

        //        }
        //        return Ok(new ResponseObject
        //        {
        //            Success = true,
        //            Message = "Tải thành công",
        //            Data = data
        //        });
        //    }
        //    else
        //    {
        //        return BadRequest("Không tìm thấy tệp tin hoặc tệp tin trống.");
        //    }
        //}

    }
}
