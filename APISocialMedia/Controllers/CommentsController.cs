﻿using APISocialMedia.Data.Entities;
using APISocialMedia.Helps;
using APISocialMedia.Services;
using APISocialMedia.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace APISocialMedia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly UserManager<AppUser> _userManager;

        public CommentsController(ICommentService commentService, UserManager<AppUser> userManager)
        {
            _commentService = commentService;
            _userManager = userManager;
        }
        [HttpGet("{postId}")]
        public async Task<IActionResult> GetCommentByPost(int postId)
        {
            var comment = await _commentService.GetCommentByPost(postId);
            if(comment.Count == 0)
            {
                return Ok(new ResponseObject
                {
                    Success = true,
                    Message = "No comment."
                });
            }
            return Ok(new ResponseObject
            {
                Success = true,
                Message = "List comment.",
                Data = comment
            });
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(CommentRequest commentRequest)
        {
            var user = await _userManager.FindByEmailAsync(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value!);
            var userId = user!.Id;

            var result = await _commentService.CreateComment(commentRequest, userId);
            if (result == 0)
                return NotFound();
            return Ok(commentRequest);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComment(int id)
        {
            
            var result = await _commentService.DeleteComment(id, User);
            if(result == 0)
                return BadRequest();
            return Ok("Success");
        }
    }
}
