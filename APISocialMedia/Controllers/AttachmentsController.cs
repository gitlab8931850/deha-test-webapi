﻿using APISocialMedia.Helps;
using APISocialMedia.Services;
using APISocialMedia.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APISocialMedia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AttachmentsController : ControllerBase
    {
        private readonly IAttachService _attachService;

        public AttachmentsController(IAttachService attachService)
        {
            _attachService = attachService;
        }
        [HttpGet("{postId}")]
        public async Task<IActionResult> GetAllByPost(int postId)
        {
            List<AttachViewModel> attachs = new List<AttachViewModel>();
            try
            {
                attachs = await _attachService.GetAttachByPost(postId);
                if (attachs.Count == 0)
                    return NotFound();
                return Ok(attachs);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAttach(int id)
        {
            var result = await _attachService.DeleteAttach(id, User);
            if(result == 0)
                return NotFound();
            if (result == -1)
                return StatusCode(403);
            return Ok(new ResponseObject
            {
                Success = true,
            });
        }
    }
}
