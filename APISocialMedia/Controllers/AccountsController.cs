﻿using APISocialMedia.Data;
using APISocialMedia.Data.Entities;
using APISocialMedia.Helps;
using APISocialMedia.Services;
using APISocialMedia.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace APISocialMedia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly UserManager<AppUser> _userManager;

        public AccountsController(IAccountService accountService, UserManager<AppUser> userManager)
        {
            _accountService = accountService;
            _userManager = userManager;
            
        }
        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromForm]RegisterViewModel registerViewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var result = await _accountService.RegisterAsync(registerViewModel);
            if (result.Succeeded)
                return Ok();
            return StatusCode(500);
        }
        [HttpPost("Login")]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _accountService.LoginAsync(loginViewModel);
                    if (string.IsNullOrEmpty(result))
                        return Unauthorized("Sai tài khoản hoặc mật khẩu");
                    return Ok(result);
                }
                catch
                {
                    return Unauthorized("Lỗi kết nối đến csdl");
                }
            }
            return NotFound();
        }
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UserManager([FromForm]UserViewModel userViewModel)
        {
            var result = await _accountService.UpdateUser(User, userViewModel);
            if (!result.Succeeded)
                return NotFound();
            return Ok(result.Succeeded);
        }
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetUser()
        {
            var user = await _accountService.GetUserInfo(User);
            if (user == null)
                return NotFound();
            return Ok(user);
        }
        [HttpDelete("{userId}")]
        [Authorize(Roles = AppRole.Admin)]
        public async Task<IActionResult> DeleteUser(string userId)
        {
            var result = await _accountService.DeleteUser(userId);
            if (result.Succeeded)
                return Ok(result.Succeeded);
            return BadRequest();
        }
        [HttpGet("GetAllUser")]
        [Authorize(Roles = AppRole.Admin)]
        public async Task<IActionResult> GetAllUser()
        {
            var data = await _accountService.GetAllUser();
            if (data == null)
                return Ok(new ResponseObject
                {
                    Success = true,
                    Message = "Khong co user nao"
                });
            return Ok(new ResponseObject
            {
                Success = true,
                Message = "Danh sach user",
                Data = data
            });
        }
        [HttpGet("GetAllFilter")]
        [Authorize(Roles = AppRole.Admin)]
        public async Task<IActionResult> GetAllFilter(string? sortOrder, string? searchString, int? pageNumber, int pageSize = 5)
        {
            var data = await _accountService.GetAllFilter(sortOrder!,searchString!,pageNumber,pageSize);
            if (data == null)
                return Ok(new ResponseObject
                {
                    Success = true,
                    Message = "Khong co user nao"
                });
            return Ok(new ResponseObject
            {
                Success = true,
                Message = "Danh sach user",
                Data = data
            });
        }
    }
}
