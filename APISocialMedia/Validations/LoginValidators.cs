﻿using APISocialMedia.ViewModels;
using FluentValidation;

namespace APISocialMedia.Validations
{
    public class LoginValidators : AbstractValidator<LoginViewModel>
    {
        public LoginValidators()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email must not be empty.")
                .NotNull()
                .WithMessage("Email must not be null.")
                .EmailAddress()
                .WithMessage("Email is not a valid email address.");
            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password must not be empty.")
                .NotNull()
                .WithMessage("Password must not be null.");
        }
    }
}
