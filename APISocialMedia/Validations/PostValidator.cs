﻿using APISocialMedia.ViewModels;
using FluentValidation;

namespace APISocialMedia.Validations
{
    public class PostViewModelValidator : AbstractValidator<PostViewModel>
    {
        public PostViewModelValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                .WithMessage("Id must not be empty.")
                .NotNull()
                .WithMessage("Id must not be null.");
            RuleFor(x => x.Title)
                .NotEmpty()
                .WithMessage("Title must not be empty.")
                .NotNull()
                .WithMessage("Title must not be null.")
                .MinimumLength(3)
                .WithMessage("The length of ‘Title’ must be at least 3 characters.");
            RuleFor(x => x.Content)
                .NotEmpty()
                .WithMessage("Content must not be empty.")
                .NotNull()
                .WithMessage("Content must not be null.")
                .MinimumLength(3)
                .WithMessage("The length of ‘Content’ must be at least 3 characters.");
        }
    }
    public class PostRequestValidator : AbstractValidator<PostRequest>
    {
        public PostRequestValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .WithMessage("Title must not be empty.")
                .NotNull()
                .WithMessage("Title must not be null.")
                .MinimumLength(3)
                .WithMessage("The length of ‘Title’ must be at least 3 characters.");
            RuleFor(x => x.Content)
                .NotEmpty()
                .WithMessage("Content must not be empty.")
                .NotNull()
                .WithMessage("Content must not be null.")
                .MinimumLength(3)
                .WithMessage("The length of ‘Content’ must be at least 3 characters.");
        }
    }
}
