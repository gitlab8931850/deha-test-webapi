﻿using APISocialMedia.ViewModels;
using FluentValidation;

namespace APISocialMedia.Validations
{
    public class UserValidation : AbstractValidator<UserViewModel>
    {
        public UserValidation()
        {
            RuleFor(x => x.FullName)
                .NotEmpty()
                .WithMessage("FullName must not be empty.")
                .NotNull()
                .WithMessage("FullName must not be null.");
            RuleFor(x => x.Dob)
                .NotEmpty()
                .WithMessage("Dob must not be empty.")
                .NotNull()
                .WithMessage("Dob must not be null.");
        }
    }
}
