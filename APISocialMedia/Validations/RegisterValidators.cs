﻿using APISocialMedia.ViewModels;
using FluentValidation;

namespace APISocialMedia.Validations
{
    public class RegisterValidators :AbstractValidator<RegisterViewModel>
    {
        public RegisterValidators()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email must not be empty.")
                .NotNull()
                .WithMessage("Email must not be null.")
                .EmailAddress()
                .WithMessage("Email is not a valid email address.");
            RuleFor(x => x.FullName)
                .NotEmpty()
                .WithMessage("FullName must not be empty.")
                .NotNull()
                .WithMessage("FullName must not be null.");
            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password must not be empty.")
                .NotNull()
                .WithMessage("Password must not be null.")
                .Matches(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d\W_]{6,}$")
                .WithMessage("Password has at least 6 characters, one uppercase letter, one lowercase letter, and one number");
            RuleFor(x => x.ConfirmPassword)
                .NotEmpty()
                .WithMessage("Confirm password must not be empty.")
                .Equal(x => x.Password)
                .WithMessage("Password should match");
        }
    }
}
