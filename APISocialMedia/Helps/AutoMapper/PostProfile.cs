﻿using APISocialMedia.Data.Entities;
using APISocialMedia.ViewModels;
using AutoMapper;

namespace APISocialMedia.Helps.AutoMapper
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<Post, PostViewModel>().ReverseMap();
            CreateMap<PostRequest, Post>();
            CreateMap<PostResponse, Post>().ReverseMap();
        }
    }
}
