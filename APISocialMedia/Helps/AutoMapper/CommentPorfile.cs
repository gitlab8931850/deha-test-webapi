﻿using APISocialMedia.Data.Entities;
using APISocialMedia.ViewModels;
using AutoMapper;

namespace APISocialMedia.Helps.AutoMapper
{
    public class CommentPorfile : Profile
    {
        public CommentPorfile()
        {
            CreateMap<CommentRequest, Comment>();
            CreateMap<Comment,CommentViewModel>().ReverseMap();
        }
    }
}
