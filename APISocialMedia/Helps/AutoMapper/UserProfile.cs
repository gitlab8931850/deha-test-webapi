﻿using APISocialMedia.Data.Entities;
using APISocialMedia.ViewModels;
using AutoMapper;

namespace APISocialMedia.Helps.AutoMapper
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserViewModel, AppUser>().ReverseMap();
            CreateMap<AppUser, UserInfo>().ReverseMap();
        }
    }
}
