﻿using APISocialMedia.Data.Entities;
using APISocialMedia.ViewModels;
using AutoMapper;

namespace APISocialMedia.Helps.AutoMapper
{
    public class RegisterProfile : Profile
    {
        public RegisterProfile()
        {
            CreateMap<RegisterViewModel, AppUser>();
        }
    }
}
