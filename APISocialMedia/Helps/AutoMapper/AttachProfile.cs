﻿using APISocialMedia.Data.Entities;
using APISocialMedia.ViewModels;
using AutoMapper;

namespace APISocialMedia.Helps.AutoMapper
{
    public class AttachProfile : Profile
    {
        public AttachProfile()
        {
            CreateMap<AttachRequest, Attachment>();
            CreateMap<AttachViewModel,Attachment>().ReverseMap();
        }
    }
}
