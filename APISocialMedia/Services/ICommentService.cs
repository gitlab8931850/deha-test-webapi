﻿using APISocialMedia.Helps;
using APISocialMedia.ViewModels;
using System.Security.Claims;

namespace APISocialMedia.Services
{
    public interface ICommentService
    {
        public Task<List<CommentViewModel>> GetCommentByPost(int  postId);
        public Task<int> CreateComment(CommentRequest commentRequest, string userId);
        public Task<int> UpdateComment(CommentViewModel commentViewModel);
        public Task<int> DeleteComment(int cmtId, ClaimsPrincipal User);
        public Task<PaginatedList<CommentViewModel>> GetCommentByPostFilter(int postId, int? pageNumber, int pageSize);
    }
}
