﻿using APISocialMedia.Data.Entities;
using APISocialMedia.ViewModels;
using System.Security.Claims;
using APISocialMedia.Helps;
namespace APISocialMedia.Services
{
    public interface IPostService
    {
        Task<PaginatedList<PostResponse>> GetAllFilter(string sortOrder, string currentFilter, string searchString, string? userId, int? pageNumber, int pageSize);
        public Task<List<Post>> GetAsync();
        public Task<Post> GetByIdAsync(int id, int? pageNumber, int pageSize);
        public Task<int> AddPost(PostRequest postRequest, string userId);
        public Task<int> UpdatePost(PostViewModel postViewModel, ClaimsPrincipal user);
        public Task<int> DeletePost(int id, ClaimsPrincipal user);
        public Task<int> UpVote(int postId, string userId);
        public Task<int> UpView(int postId, ClaimsPrincipal user);
        public Task AddListAttachByPost(IList<IFormFile> files, int postId);
    }
}
