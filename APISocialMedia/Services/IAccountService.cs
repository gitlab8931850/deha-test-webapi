﻿using APISocialMedia.Data.Entities;
using APISocialMedia.Helps;
using APISocialMedia.ViewModels;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace APISocialMedia.Services
{
    public interface IAccountService
    {
        public Task<IdentityResult> RegisterAsync(RegisterViewModel model);
        public Task<string> LoginAsync(LoginViewModel model);
        public Task<IdentityResult> UpdateUser(ClaimsPrincipal user, UserViewModel model);
        public Task<UserInfo> GetUserInfo(ClaimsPrincipal user);
        public Task<IdentityResult> DeleteUser(string userId);
        public Task<List<AppUser>> GetAllUser();
        public Task<PaginatedList<AppUser>> GetAllFilter(string sortOrder, string searchString, int? pageNumber, int pageSize);
        public Task<bool> CheckAuth(string userIdFromAny, ClaimsPrincipal user);
        public bool CheckAdmin(ClaimsPrincipal user);
    }
}
