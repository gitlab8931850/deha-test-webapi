﻿using APISocialMedia.Data;
using APISocialMedia.Data.Entities;
using APISocialMedia.Helps;
using APISocialMedia.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace APISocialMedia.Services
{
    public class AccountServices : IAccountService
    {
        public readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IDistributedCache _cache;
        public AccountServices(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            IConfiguration configuration,
            IMapper mapper,
            RoleManager<IdentityRole> roleManager,
            IDistributedCache cache
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _mapper = mapper;
            _roleManager = roleManager;
            _cache = cache;
        }
        public async Task<string> LoginAsync(LoginViewModel model)
        {
            // Check user
            var user = await _userManager.FindByEmailAsync(model.Email!);
            if (user == null)
            {
                return string.Empty;
            }
            if(user.IsDeleted)
            {
                return "Banned";
            }
            // Check password
            var passwordValid = await _userManager.CheckPasswordAsync(user, model.Password!);
            if (!passwordValid)
            {
                return string.Empty;
            }
            // List claim
            var auth = new List<Claim>
            {
                new Claim(ClaimTypes.Email, model.Email!),
                new Claim(JwtRegisteredClaimNames.Jti, new Guid().ToString()),
                new Claim(ClaimTypes.GivenName, user.FullName!)
            };
            // Add role to claimList
            var userRoles = await _userManager.GetRolesAsync(user);
            foreach (var role in userRoles)
            {
                auth.Add(new Claim(ClaimTypes.Role, role.ToString()));
            }
            var authenKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]!));
            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddHours(1),
                claims: auth,
                signingCredentials: new SigningCredentials(authenKey, SecurityAlgorithms.HmacSha256Signature)
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<IdentityResult> RegisterAsync(RegisterViewModel model)
        {
            await _cache.RemoveAsync("userList");
            var user = _mapper.Map<AppUser>(model);
            user.UserName = model.Email;
            user.DateCreated = DateTime.UtcNow;
            var result = await _userManager.CreateAsync(user, model.Password!);

            if (result.Succeeded)
            {
                if (!await _roleManager.RoleExistsAsync(AppRole.Member))
                    await _roleManager.CreateAsync(new IdentityRole(AppRole.Member));
                await _userManager.AddToRoleAsync(user, AppRole.Member);
            }
            return result;
        }
        
        public async Task<UserInfo> GetUserInfo(ClaimsPrincipal user)
        {
            var email = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            var u2 = await _userManager.Users.AsNoTracking().FirstOrDefaultAsync(x => x.Email == email!);
            var u = _mapper.Map<UserInfo>(u2);
            return u != null ? u : null!;
        }

        public async Task<IdentityResult> UpdateUser(ClaimsPrincipal user, UserViewModel model)
        {
            await _cache.RemoveAsync("userList");
            var email = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            var u = await _userManager.FindByEmailAsync(email!);
            u!.FullName = model.FullName;
            u.PhoneNumber = model.PhoneNumber;
            u.Dob = model.Dob;
            var result = await _userManager.UpdateAsync(u);
            return result;
        }
        public async Task<IdentityResult> DeleteUser(string userId)
        {
            await _cache.RemoveAsync("userList");
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
                user.IsDeleted = true;
            return await _userManager.UpdateAsync(user!);
        }
        public async Task<List<AppUser>> GetAllUser()
        {
            // return await _userManager.Users.ToListAsync();
            var userInCache = _cache.GetString("userList");
            if (userInCache != null)
            {
                var users = JsonConvert.DeserializeObject<List<AppUser>>(userInCache);
                return users!;
            }
            else
            {
                var posts = await _userManager.Users.ToListAsync();
                var values = JsonConvert.SerializeObject(posts);
                var options = new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1)
                };
                _cache.SetString("userList", values, options);
                return posts;
            }
        }
        public async Task<PaginatedList<AppUser>> GetAllFilter(string sortOrder, string searchString, int? pageNumber, int pageSize)
        {
            var users = await GetAllUser();
            var sources = _mapper.Map<IEnumerable<AppUser>>(users);

            if (!string.IsNullOrEmpty(searchString))
            {
                sources = sources.Where(s => s.UserName!.Contains(searchString));
            }

            sources = sortOrder switch
            {
                "name" => sources.OrderBy(s => s.FullName),
                "name_desc" => sources.OrderByDescending(s => s.FullName),
                "email" => sources.OrderBy(s => s.Email),
                "email_desc" => sources.OrderByDescending(s => s.Email),
                "date_created" => sources.OrderBy(s => s.DateCreated),
                _ => sources.OrderByDescending(s => s.DateCreated),
            };
            return PaginatedList<AppUser>.Create(_mapper.Map<IEnumerable<AppUser>>(sources), pageNumber ?? 1, pageSize);
        }
        public async Task<bool> CheckAuth(string userIdFromAny, ClaimsPrincipal user)
        {
            var findUser = await GetUserInfo(user);
            if (findUser == null) return false;
            if(findUser.Id == userIdFromAny) return true;
            return false;
        }
        public bool CheckAdmin(ClaimsPrincipal user)
        {
            return user.IsInRole(AppRole.Admin);
        }
    }
}
