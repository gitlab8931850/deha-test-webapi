﻿using APISocialMedia.ViewModels;
using System.Security.Claims;

namespace APISocialMedia.Services
{
    public interface IAttachService
    {
        public Task<List<AttachViewModel>> GetAttachByPost(int postId);
        public Task<int> AddAttach(AttachRequest attachRequest);
        public Task<int> UpdateAttachByPost(AttachViewModel attachViewModel, int postId);
        public Task<int> DeleteAttach(int attachId, ClaimsPrincipal user);
        public Task<int> DeleteAttachByPost(int postId);
    }
}
