﻿using APISocialMedia.Data;
using APISocialMedia.Data.Entities;
using APISocialMedia.Helps;
using APISocialMedia.ViewModels;
using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System.Drawing.Printing;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace APISocialMedia.Services
{

    public class PostServices : IPostService
    {
        private readonly SocialMediaDbContext _context;
        private readonly IMapper _mapper;
        private readonly IStorageService _storageService;
        private const string USER_CONTENT_FOLDER_NAME = "Uploads/user-content";
        private readonly IAttachService _attachService;
        private readonly IAccountService _accountService;
        private readonly IDistributedCache _cache;
        private readonly ICommentService _commentService;
        public PostServices(
            SocialMediaDbContext context, 
            IMapper mapper,
            IStorageService storageService,
            IAttachService attachService,
            IAccountService accountService,
            IDistributedCache cache,
            ICommentService commentService
            )
        {
            _context = context;
            _mapper = mapper;
            _storageService = storageService;
            _attachService = attachService;
            _accountService = accountService;
            _cache = cache;
            _commentService = commentService;
        }
        public async Task<int> AddPost(PostRequest postRequest, string userId)
        {
            await _cache.RemoveAsync("postList");
            var post = _mapper.Map<Post>(postRequest);
            //post.UserId = userId;
            var u = _context.Users.FirstOrDefault(u => u.Id == userId);
            post.User = u;
            var entryPost = await _context.Posts.AddAsync(post);
            var result = await _context.SaveChangesAsync();
            var postId = entryPost.Entity.Id;

            if (postRequest.Files != null)
            {
                var files = postRequest.Files;
                await AddListAttachByPost(files, postId);
            }
            return result;
        }
        private async Task<string> SaveFile(IFormFile file)
        {
            var originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName!.Trim('"');
            var fileName = $"{Guid.NewGuid()}{Path.GetExtension(originalFileName)}";
            await _storageService.SaveFileAsync(file.OpenReadStream(), fileName);
            return "/" + USER_CONTENT_FOLDER_NAME + "/" + fileName;
        }
        public async Task<int> DeletePost(int id, ClaimsPrincipal user)
        {
            await _cache.RemoveAsync("postList");
            var post = await _context.Posts.FindAsync(id);
            //if(post == null)
            //    return 0;
            if (!(await _accountService.CheckAuth(post!.UserId!, user)) && !user.IsInRole(AppRole.Admin))
                return -1;
            await _attachService.DeleteAttachByPost(post.Id);
            _context.Posts.Remove(post);
            return await _context.SaveChangesAsync();
        }

        public async Task<List<Post>> GetAsync()
        {
            
            //return posts.Count() > 0 ? posts : null!;
            var postInCache = _cache.GetString("postList");
            if (postInCache != null)
            {
                var posts = JsonConvert.DeserializeObject<List<Post>>(postInCache);
                return posts!;
            }
            else
            {
                List<Post> posts = new List<Post>();
                try
                {
                    posts = await _context.Posts.ToListAsync();
                }
                catch
                {
                    return null!;
                }
                var values = JsonConvert.SerializeObject(posts);
                var options = new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1)
                };
                _cache.SetString("postList", values, options);
                return posts;
            }
        }

        public async Task<Post> GetByIdAsync(int id, int ?pageNumber, int pageSize)
        {
            var post = await _context.Posts.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            if (post == null)   
                return null!;
            var comments = await _commentService.GetCommentByPostFilter(post.Id, pageNumber, pageSize);
            post.Comments = _mapper.Map<List<Comment>>(comments);
            return post!;
        }

        public async Task<int> UpdatePost(PostViewModel postViewModel, ClaimsPrincipal user)
        {
            await _cache.RemoveAsync("postList");
            var post = await _context.Posts.FindAsync(postViewModel.Id);
            if (post == null)
                return 0;
            if (!(await _accountService.CheckAuth(post.UserId!, user)))
                return -1;
            post.Title = postViewModel.Title;
            post.Content = postViewModel.Content;
            if(postViewModel.Files != null)
            {
                await _attachService.DeleteAttachByPost(post.Id);
                var  files = postViewModel.Files;
                await AddListAttachByPost(files, post.Id);
            }
            _context.Posts.Update(post);
            return await _context.SaveChangesAsync();
        }
        //public async Task<int> AddAttachToPost(int postId, IFormFile file)
        //{
        //    var post = await _context.Posts.FindAsync(postId);
        //    if (post == null)
        //        return 0;
        //    var attach = new AttachRequest
        //    {
        //        Title = file.FileName,
        //        FileUrl = await SaveFile(file),
        //        FileSize = file.Length,
        //        PostId = postId,
        //        AttachFile = file,
        //    };
        //    var result = await _attachService.AddAttach(attach);
        //    return result;
        //}
        public async Task AddListAttachByPost(IList<IFormFile> files, int postId)
        {
            foreach (var file in files)
            {
                var attach = new AttachRequest
                {
                    Title = file.FileName,
                    FileUrl = await SaveFile(file),
                    FileSize = file.Length,
                    PostId = postId,
                    AttachFile = file,
                };
                await _attachService.AddAttach(attach);
            }

        }
        public async Task<int> UpVote(int postId, string userId)
        {
            await _cache.RemoveAsync("postList");
            var vote = new VotesInPost { PostId = postId, UserId = userId };
            var post = await _context.Posts.FindAsync(postId);
            if (post == null)
                return 0;
            // Kiem tra trc do da vote chua
            var voteExist = await _context.VotesInPosts.FirstOrDefaultAsync(x => x.PostId == postId && x.UserId == userId);
            if(voteExist != null)
            {
                post.Votes -= 1;
                //_context.Posts.Update(post);
                _context.VotesInPosts.Remove(voteExist);
                return await _context.SaveChangesAsync();
            }
            post.Votes += 1;
            //_context.Posts.Update(post);
            await _context.VotesInPosts.AddAsync(vote);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> UpView(int postId, ClaimsPrincipal user)
        {
            await _cache.RemoveAsync("postList");
            if (user == null) return 0;
            var post = await _context.Posts.FirstOrDefaultAsync(x => x.Id == postId);
            var email = user.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value;
            var userApp = await _context.Users.AsNoTracking().FirstOrDefaultAsync(x => x.Email == email);
            if (post == null) return -1;
            if (post.UserId != userApp!.Id)
            {
                post.ViewCount++;
                return await _context.SaveChangesAsync();
            }
            return 0;
        }

        public async Task<PaginatedList<PostResponse>> GetAllFilter(string sortOrder,string currentFilter, string searchString, string? userId, int? pageNumber, int pageSize)
        {
            var post = await GetAsync();
            var sources = _mapper.Map<IEnumerable<PostResponse>>(post);
            if(userId != null)
            {
                sources = sources.Where(x => x.UserId == userId);
            }
            if (!string.IsNullOrEmpty(searchString))
            {
                sources = sources.Where(s => s.Title!.Contains(searchString));
            }

            sources = sortOrder switch
            {
                "title" => sources.OrderBy(s => s.Title),
                "title_desc" => sources.OrderByDescending(s => s.Title),
                "date_created" => sources.OrderBy(s => s.DateCreated),
                "votes_desc" => sources.OrderByDescending(s => s.Votes),
                "votes" => sources.OrderBy(s => s.Votes),
                _ => sources.OrderByDescending(s => s.DateCreated),
            };
            return PaginatedList<PostResponse>.Create(_mapper.Map<IEnumerable<PostResponse>>(sources), pageNumber ?? 1, pageSize);
        }
    }
}
