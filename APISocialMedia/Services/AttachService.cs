﻿using APISocialMedia.Data;
using APISocialMedia.Data.Entities;
using APISocialMedia.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace APISocialMedia.Services
{
    public class AttachService : IAttachService
    {
        private readonly IStorageService _storageService;
        private readonly SocialMediaDbContext _context;
        private readonly IMapper _mapper;
        private const string USER_CONTENT_FOLDER_NAME = "Uploads/user-content";
        private readonly UserManager<AppUser> _userManager;

        public AttachService(
            IStorageService storageService, 
            SocialMediaDbContext context, 
            IMapper mapper,
            UserManager<AppUser> userManager)
        {
            _storageService = storageService;
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
        }
        public async Task<int> AddAttach(AttachRequest attachRequest)
        {
            var attach = _mapper.Map<Attachment>(attachRequest);
            await _context.Attachments.AddAsync(attach);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> DeleteAttachByPost(int postId)
        {
            var attach = await _context.Attachments.Where(x => x.PostId == postId).ToListAsync();
            if(attach != null)
            {
                foreach(var attachment in attach)
                {
                    await _storageService.DeleteFileAsync(attachment.FileUrl!.Replace("/"+USER_CONTENT_FOLDER_NAME +"/",""));
                    _context.Attachments.Remove(attachment);
                }
                return await _context.SaveChangesAsync();
            }
            return await _context.SaveChangesAsync();
        }

        public async Task<List<AttachViewModel>> GetAttachByPost(int postId)
        {
            var attach = _mapper.Map<List<AttachViewModel>>(await _context.Attachments.Where(x => x.PostId ==postId).ToListAsync());
            return attach;
        }
        public async Task<int> DeleteAttach(int attachId, ClaimsPrincipal user)
        {
            var email = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
            var u = await _context.Users.FirstOrDefaultAsync(x => x.Email == email);
            var attach = await _context.Attachments.FindAsync(attachId);
            if (attach == null)
                return 0;
            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == attach.PostId);
            if (post!.UserId != u!.Id)
                return -1;
            _context.Attachments.Remove(attach);
            return await _context.SaveChangesAsync();
        }

        public Task<int> UpdateAttachByPost(AttachViewModel attachViewModel, int postId)
        {
            throw new NotImplementedException();
        }
        
    }
}
