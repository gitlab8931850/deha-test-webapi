﻿using APISocialMedia.Data;
using APISocialMedia.Data.Entities;
using APISocialMedia.Helps;
using APISocialMedia.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace APISocialMedia.Services
{
    public class CommentService : ICommentService
    {
        private readonly SocialMediaDbContext _context;
        private readonly IMapper _mapper;
        private readonly IAccountService _accountService;
        private readonly UserManager<AppUser> _userManager;
        public CommentService(
            SocialMediaDbContext context,
            IMapper mapper,
            IAccountService accountService,
            UserManager<AppUser> userManager
            )
        {
            _mapper = mapper;
            _context = context;
            _accountService = accountService;
            _userManager = userManager;
        }
        public async Task<int> CreateComment(CommentRequest commentRequest, string userId)
        {
            var comment = _mapper.Map<Comment>(commentRequest);
            comment.UserId = userId;
            comment.DateCreated = DateTime.UtcNow.AddHours(7);
            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == comment.PostId);
            if(post == null)
                return 0;
            await _context.Comments.AddAsync(comment);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteComment(int cmtId, ClaimsPrincipal User)
        {
            var user = await _userManager.FindByEmailAsync(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value!);
            var comment = await _context.Comments.FindAsync(cmtId);
            if(comment != null && (comment.UserId == user!.Id || User.IsInRole(AppRole.Admin)))
            {
                _context.Comments.Remove(comment);
                return await _context.SaveChangesAsync();
            }
            return 0;
        }

        public async Task<List<CommentViewModel>> GetCommentByPost(int postId)
        {
            var comments = await _context.Comments.Where(c => c.PostId == postId).ToListAsync();
            return _mapper.Map<List<CommentViewModel>>(comments);
        }
        public async Task<PaginatedList<CommentViewModel>> GetCommentByPostFilter(int postId, int? pageNumber, int pageSize)
        {
            var comments = await _context.Comments.Where(c => c.PostId == postId).ToListAsync();
            var sources = _mapper.Map<List<CommentViewModel>>(comments);
            return PaginatedList<CommentViewModel>.Create(sources, pageNumber ?? 1, pageSize);
        }

        public async Task<int> UpdateComment(CommentViewModel commentViewModel)
        {
            var comment = await _context.Comments.Where(
                x => x.PostId == commentViewModel.PostId &&
                x.UserId == commentViewModel.UserId
            ).FirstOrDefaultAsync();

            if(comment != null)
            {
                comment.Content = commentViewModel.Content;
                _context.Comments.Update(comment);
                return await _context.SaveChangesAsync();
            }
            return 0;
        }
    }
}
