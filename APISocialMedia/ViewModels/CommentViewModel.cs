﻿using APISocialMedia.Data.Entities;

namespace APISocialMedia.ViewModels
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string? UserId { get; set; }
        public string? Content { get; set; }
        public DateTime DateCreated { get; set; }
    }
    public class CommentRequest
    {
        public int PostId { get; set; }
        public string? Content { get; set; }
    }
}
