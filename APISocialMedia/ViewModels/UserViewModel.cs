﻿namespace APISocialMedia.ViewModels
{
    public class UserViewModel
    {
        public string? FullName { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime? Dob { get; set; }
    }
    public class UserInfo
    {
        public string? Id { get; set; }
        public string? UserName { get; set;}
        public string? Email { get; set;}
        public string? FullName { get; set;}
        public string? PhoneNumber { get; set;}
        public DateTime? Dob { get; set;}

    }
}
