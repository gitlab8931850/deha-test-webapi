﻿using APISocialMedia.Data.Entities;
using FluentValidation;

namespace APISocialMedia.ViewModels
{
    public class PostViewModel 
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Content { get; set; }
        public IList<IFormFile>? Files { get; set; }
    }
    public class PostResponse
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Content { get; set; }
        public DateTime DateCreated { get; set; }
        public int ViewCount { get; set; }
        public int Votes { get; set; }
        public string? UserId { get; set; }
    }
    public class PostRequest
    {
        public string? Title { get; set; }
        public string? Content { get; set; }
        public IList<IFormFile>? Files { get; set; }
    }
}
