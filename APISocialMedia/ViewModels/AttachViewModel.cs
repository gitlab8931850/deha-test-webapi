﻿using APISocialMedia.Data.Entities;

namespace APISocialMedia.ViewModels
{
    public class AttachViewModel
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? FileUrl { get; set; }
        public long? FileSize { get; set; }
        public int PostId { get; set; }
    }
    public class AttachRequest
    {
        public IFormFile? AttachFile { get; set; }
        public string? Title { get; set; }
        public string? FileUrl { get; set; }
        public long? FileSize { get; set; }
        public int PostId { get; set; }
    }
}
