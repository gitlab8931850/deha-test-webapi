﻿using APISocialMedia.Controllers;
using APISocialMedia.Data.Entities;
using APISocialMedia.Helps;
using APISocialMedia.Services;
using APISocialMedia.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Moq;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace UnitTest.Controllers
{
    public class PostController
    {
        Mock<IMapper> _mapperMock = new Mock<IMapper>();
        Mock<IPostService> _postServiceMock = new Mock<IPostService>();
        private static Mock<IUserStore<AppUser>> _userStoreMock = new Mock<IUserStore<AppUser>>();
        Mock<UserManager<AppUser>>  _userManagerMock = new Mock<UserManager<AppUser>>(_userStoreMock.Object, null!, null!, null!, null!, null!, null!, null!, null!);        
        [Fact]
        public async Task GetAll_ReturnListPost()
        {
            // Arrange
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);

            // Mock the response from the postService.GetAsync() method
            var posts = new List<Post>()
            {
                new Post { 
                    Id = 1, 
                    Title = "Test title 1", 
                    Content = "Test content 1", 
                    UserId = "Test userId 1", 
                    Comments = new List<Comment>()
                    {
                        new Comment { Id = 1, PostId = 1, UserId = "Test userId 1", Content = "test cmt from user 1" },
                        new Comment { Id = 2, PostId = 1, UserId = "Test userId 2", Content = "test cmt from user 2" },
                    }
                },
                new Post {
                    Id = 2,
                    Title = "Test title 2",
                    Content = "Test content 2",
                    UserId = "Test userId 2",
                    Comments = new List<Comment>()
                    {
                        new Comment { Id = 3, PostId = 2, UserId = "Test userId 1", Content = "test cmt from user 1" },
                        new Comment { Id = 4, PostId = 2, UserId = "Test userId 2", Content = "test cmt from user 2" },
                    }
                }
            };
            List<PostResponse> postResponse = new List<PostResponse>();
            _postServiceMock.Setup(mock => mock.GetAsync()).ReturnsAsync(posts);
            _mapperMock.Setup(mock => mock.Map<List<PostResponse>>(posts)).Returns(postResponse);
            // Act
            var result = await controller.GetAll();

            // Assert
            var objectResult = Assert.IsType<OkObjectResult>(result);
            var responseObject = Assert.IsType<ResponseObject>(objectResult.Value);
            Assert.True(responseObject.Success);
            Assert.Equal("Danh sach bai viet", responseObject.Message);
            Assert.Equal(postResponse, responseObject.Data);
        }
        [Fact]
        public async Task GetAll_ReturnListPostFailure()
        {
            // Arrange
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);

            // Mock the response from the postService.GetAsync() method
            List<Post> posts = null!;

            _postServiceMock.Setup(mock => mock.GetAsync()).ReturnsAsync(posts);

            // Act
            var result = await controller.GetAll();

            // Assert
            var objectResult = Assert.IsType<OkObjectResult>(result);
            var responseObject = Assert.IsType<ResponseObject>(objectResult.Value);
            Assert.True(!responseObject.Success);
            Assert.Equal("Không có bài viết nào", responseObject.Message);
        }
        [Fact]
        public async Task PostController_GetAllFilter()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            List<PostResponse> items = new List<PostResponse>
            {
                new PostResponse(),
                new PostResponse(),
                new PostResponse()
            };
            int pageNumber = 2;
            int pageSize = 2;
            
            PaginatedList<PostResponse> postResponse = PaginatedList<PostResponse>.Create(items, pageNumber, pageSize);
            _postServiceMock.Setup(m => m.GetAllFilter(null!, null!, null!, null, pageNumber, pageSize)).ReturnsAsync(postResponse);

            var result = await controller.GetAllFilter(null,null,null,null, pageNumber, pageSize);

            var okObjectResult = Assert.IsType<OkObjectResult>(result);
            var responseObject = Assert.IsType<PaginatedList<PostResponse>>(okObjectResult.Value);
            Assert.Single(responseObject);
        }
        [Fact]
        public async Task PostController_DeletePost_Notfound()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);

            var user = new ClaimsPrincipal();
            _postServiceMock.Setup(m => m.DeletePost(1, user)).ReturnsAsync(0);
            var result = await controller.DeletePost(1);

            var objectResult = Assert.IsType<StatusCodeResult>(result);
            Assert.Equal(404, objectResult.StatusCode);
        }
        [Fact]
        public async Task PostController_DeletePost_Forbidden()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);

            int postId = 123;
            _postServiceMock.Setup(m => m.DeletePost(postId, It.IsAny<ClaimsPrincipal>())).ReturnsAsync(-1);
            var result = await controller.DeletePost(postId);

            var objectResult = Assert.IsType<StatusCodeResult>(result);
            Assert.Equal(403, objectResult.StatusCode);
        }
        [Fact]
        public async Task PostController_DeletePost_Ok()
        {
            // Arrange
            var postId = 123;
            _postServiceMock.Setup(m => m.DeletePost(postId, It.IsAny<ClaimsPrincipal>())).ReturnsAsync(1);
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);

            // Act
            var result = await controller.DeletePost(postId);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Success", okResult.Value);
        }
        [Fact]
        public async Task PostsController_AddPost_Return_Ok()
        {
            // Arrange
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            var postRequest = new PostRequest { Title = "Test title", Content = "Test content", Files = null };
            var emailClaim = new Claim(ClaimTypes.Email, "test@test.com");
            var userMock = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] { emailClaim }));
            controller.ControllerContext.HttpContext = new DefaultHttpContext { User = userMock };
            var user = new AppUser { Email = "testUser@gmail.com", Id = "testUserId" };

            _userManagerMock.Setup(m => m.FindByEmailAsync("test@test.com")).ReturnsAsync(user);
            _postServiceMock.Setup(m => m.AddPost(postRequest, user.Id)).ReturnsAsync(1);

            // Act
            var result = await controller.AddPost(postRequest);

            // Assert
            var okObject = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(postRequest, okObject.Value);
        }
        [Fact]
        public async Task PostController_AddPost_ReturnInternalServerError()
        {
            // Arange
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            var postRequest = new PostRequest { Title = "Test title", Content = "Test content", Files = null };
            var emailClaim = new Claim(ClaimTypes.Email, "Test@gmail.com");
            var userMock = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] { emailClaim }));
            var user = new AppUser { Id = "testUserId" };
            controller.ControllerContext.HttpContext = new DefaultHttpContext { User = userMock };

            _userManagerMock.Setup(m => m.FindByEmailAsync("Test@gmail.com")).ReturnsAsync(user);
            _postServiceMock.Setup(m => m.AddPost(postRequest, user.Id)).ReturnsAsync(0);

            // Act
            var result = await controller.AddPost(postRequest);

            var objectResult = Assert.IsType<StatusCodeResult>(result);
            Assert.Equal(500, objectResult.StatusCode);
        }
        [Fact]
        public async Task PostsController_AddPost_Return_BadRequest()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            var postRequest = new PostRequest { Title = null, Content = "Content" };
            controller.ModelState.AddModelError("Title", "The title must be not null.");

            var result = await controller.AddPost(postRequest);

            Assert.IsType<BadRequestResult>(result);
        }
        [Fact]
        public async Task PostsController_UpVote_Ok()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            var postId = 123;
            var emailClaim = new Claim(ClaimTypes.Email, "Test@gmail.com");
            var userMock = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] { emailClaim }));
            var user = new AppUser { Id = "testUserId" };
            controller.ControllerContext.HttpContext = new DefaultHttpContext { User = userMock };
            _userManagerMock.Setup(m => m.FindByEmailAsync("Test@gmail.com")).ReturnsAsync(user);
            _postServiceMock.Setup(m => m.UpVote(postId, user.Id)).ReturnsAsync(1);

            // Act
            var result = await controller.UpVotes(postId);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Success", objectResult.Value);
        }
        [Fact]
        public async Task PostsController_UpVote_NotFound()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            var postId = 123;
            var emailClaim = new Claim(ClaimTypes.Email, "Test@gmail.com");
            var userMock = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] { emailClaim }));
            var user = new AppUser { Id = "testUserId" };
            controller.ControllerContext.HttpContext = new DefaultHttpContext { User = userMock };
            _userManagerMock.Setup(m => m.FindByEmailAsync("Test@gmail.com")).ReturnsAsync(user);
            _postServiceMock.Setup(m => m.UpVote(postId, user.Id)).ReturnsAsync(0);

            // Act
            var result = await controller.UpVotes(postId);

            // Assert
            Assert.IsType<NotFoundResult>(result);
        }
        [Fact]
        public async Task PostsController_Update_Return_InternalServerError()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            var postViewModel = new PostViewModel { Id = 1, Content = "Test content", Title = "Test title" };
            var user = new AppUser { Id = "testUserId", Email = "test@gmail.com" };
            var emailClaim = new Claim(ClaimTypes.Email, "test@gmail.com");
            var userMock = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] { emailClaim }));
            controller.ControllerContext.HttpContext = new DefaultHttpContext { User = userMock };

            _postServiceMock.Setup(m => m.UpdatePost(postViewModel, userMock)).ReturnsAsync(0);

            // Act
            var result = await controller.Update(postViewModel);
            var objectResult = Assert.IsType<StatusCodeResult>(result);
            // Assert
            Assert.Equal(500, objectResult.StatusCode);
        }
        [Fact]
        public async Task PostsController_Update_Return_Forbidden()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            var postViewModel = new PostViewModel { Id = 1, Content = "Test content", Title = "Test title" };
            var user = new AppUser { Id = "testUserId", Email = "test@gmail.com" };
            var emailClaim = new Claim(ClaimTypes.Email, "test@gmail.com");
            var userMock = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] { emailClaim }));
            controller.ControllerContext.HttpContext = new DefaultHttpContext { User = userMock };

            _postServiceMock.Setup(m => m.UpdatePost(postViewModel, userMock)).ReturnsAsync(-1);

            // Act
            var result = await controller.Update(postViewModel);
            var objectResult = Assert.IsType<StatusCodeResult>(result);
            // Assert
            Assert.Equal(403, objectResult.StatusCode);
        }
        [Fact]
        public async Task PostsController_Update_Return_Ok()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            var postViewModel = new PostViewModel { Id = 1, Content = "Test content", Title = "Test title" };
            var user = new AppUser { Id = "testUserId", Email = "test@gmail.com" };
            var emailClaim = new Claim(ClaimTypes.Email, "test@gmail.com");
            var userMock = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] { emailClaim }));
            controller.ControllerContext.HttpContext = new DefaultHttpContext { User = userMock };

            _postServiceMock.Setup(m => m.UpdatePost(postViewModel, userMock)).ReturnsAsync(1);

            // Act
            var result = await controller.Update(postViewModel);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            // Assert
            Assert.Equal(postViewModel, objectResult.Value);
        }
        [Fact]
        public async Task PostsController_GetById_ReturnPost()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            // List claim
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, "test@gmail.com"),
                new Claim(JwtRegisteredClaimNames.Jti, new Guid().ToString()),
                new Claim(ClaimTypes.GivenName, "Nguyen Van Toan"),
                new Claim(ClaimTypes.Role, "Member")
            };
            var authenKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("thisisthesecretformytokenweb API"));
            var token = new JwtSecurityToken(
                issuer: "https://localhost:7152",
                audience: "https://localhost:7152",
                expires: DateTime.Now.AddHours(1),
                claims: claims,
                signingCredentials: new SigningCredentials(authenKey, SecurityAlgorithms.HmacSha256Signature)
                );
            var authenToken = new JwtSecurityTokenHandler().WriteToken(token);
            var identity = new ClaimsIdentity(token.Claims, "Bearer");
            var principal = new ClaimsPrincipal(identity);
            controller.ControllerContext.HttpContext = new DefaultHttpContext() { User = principal };
            var post = new Post
            {
                Id = 1,
                Title = "Test title",
                Content = "Test content",
                Votes = 10,
                ViewCount = 20,
                UserId = "testUserId",
                DateCreated = DateTime.UtcNow.AddHours(7),
                Comments = new List<Comment>
                {
                    new Comment
                    {
                        Id = 1,
                        Content = "Test comment 1",
                        PostId = 1,
                        UserId = "testUserId4"
                    },
                    new Comment
                    {
                        Id = 2,
                        Content = "Test comment 2",
                        PostId = 1,
                        UserId = "testUserId2"
                    },
                    new Comment
                    {
                        Id = 3,
                        Content = "Test comment 3",
                        PostId = 1,
                        UserId = "testUserId3"
                    },
                    new Comment
                    {
                        Id = 4,
                        Content = "Test comment 4",
                        PostId = 1,
                        UserId = "testUserId1"
                    },
                    new Comment
                    {
                        Id = 5,
                        Content = "Test comment 5",
                        PostId = 1,
                        UserId = "testUserId1"
                    }
                }
            };
            var pageNumber = 1;
            var pageSize = 5;
            _postServiceMock.Setup(m => m.UpView(post.Id, principal)).ReturnsAsync(1);
            _postServiceMock.Setup(m => m.GetByIdAsync(post.Id, pageNumber, pageSize)).ReturnsAsync(post);

            var result = await controller.GetById(post.Id,pageNumber, pageSize);

            var resultObject = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(post, resultObject.Value);
        }
        [Fact]
        public async Task PostsController_GetById_ReturnNotFound()
        {
            var controller = new PostsController(_mapperMock.Object, _postServiceMock.Object, _userManagerMock.Object);
            var claims = new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.GivenName, "Nguyen Van Toan"),
                new Claim(ClaimTypes.AuthenticationMethod, "Bearer")
            });
            var User = new ClaimsPrincipal(claims);
            controller.ControllerContext.HttpContext = new DefaultHttpContext() { User = User };
            Post post = null!;
            var pageNumber = 1;
            var pageSize = 5;
            _postServiceMock.Setup(m => m.UpView(1, User)).ReturnsAsync(1);
            _postServiceMock.Setup(m => m.GetByIdAsync(1, pageNumber, pageSize)).ReturnsAsync(post);

            var result = await controller.GetById(1, pageNumber, pageSize);

            Assert.IsType<NotFoundResult>(result);
        }
    }
}
